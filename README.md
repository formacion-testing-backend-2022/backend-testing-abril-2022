# Cómo mejorar tu testing en backend (abril 2022)

Evento de Enpresa Digitala (Grupo SPRI) impartido en formato online el 07 de abril 2022.

A continuación dejamos disponibles todos los recursos utilizados durante la jornada de formación.

## Código fuente y transparencias

**Código fuente**

Se han utilizado dos servicios. Ambos se incluyen en el repositorio actual, cada uno en una carpeta: *farmacia-api* y *almacen-api*. El código está en dos ramas, que se corresponde con cada una de las partes en que se ha dividido la jornada.

Para probar y hacer modificaciones es necesario:
- Jdk 11
- Docker
- Postgres


**Acceso a las transparencias en PDF**

Las transparencias pueden descargarse desde [aquí](/docs/transparencias.pdf).  

## Enlaces de interés

### Personas mencionadas
Hemos nombrado a algunas personas que son de interés a nivel de la comunidad de QA. A continuación os dejamos información sobre ellos por si os interesan:

 👤  &nbsp;**Carlos Blé**  
Twitter: [@carlosble](https://twitter.com/carlosble)  
Web: [carlosble.com](http://www.carlosble.com/?lang=es)



 👤  &nbsp;**Francisco Moreno**  
Twitter: [@morvader](https://twitter.com/morvader)   
Twitter: [@NorthemQuality](https://twitter.com/NorthemQuality)


 👤  &nbsp;**Javier Martín de Agar**  
Twitter: [@javimartinagar](https://twitter.com/javimartinagar)   
Web: [Mamá... ¿Qué es Scrum?](https://mamaqueesscrum.com/)

### Videos

**Las tres frases que mataron la calidad de software**  
> 📺  &nbsp;[Ver en youtube](https://www.youtube.com/watch?v=yPPCn09ys9M&t=3h47m55s)   
> _Javier Martin de Agar_. En QALovers Day 2020.

**Agile Testing**  
> 📺  &nbsp;[Ver en youtube](https://www.youtube.com/watch?v=92fI3wlyriI)  
> _Carlos Blé_. Entrevistado por Autentia.

### Bugs de software sonados

[Aquí](/docs/bugsHistoria.md) os deja un documento con las consecuencias provocadas por algunos errores en el software que vimos al comienzo de la jornada.

## Herramientas que hemos visto

A continuación os dejo el enlace de las herramientas con las que hemos ido trabajando a lo largo de la jornada así como algunas de las que sin haber profundizado hemos nombrado.

### Base
Librerías base en las que hemos basado todo el desarrollo de los tests.

| <!-- -->    | <!-- -->    |
|:----:|:----:|
| <img src="docs/imgs/logoJunit5.png" width="150px" alt="Logo de JUnit 5">           | <img src="docs/imgs/logoMockito.png" width="150px" alt="Logo de Mockito">   |
| [JUnit 5](https://junit.org/junit5/docs/current/user-guide/) | [Mockito](https://site.mockito.org/)   |

### Mockeo de estáticos/constructores
Desde la versión 3.4.0 Mockito ofrece esta posibilidad. Hay otra librería que nos ofrece también estas opciones (entre otras). Requiere tests de JUnit 4 y Mockito 2.

| <!-- -->    |
|:----:|
|<img src="docs/imgs/logoPowermock.png" width="150px" alt="Logo de Powermock">|
|[Powermock](https://github.com/powermock/powermock/wiki/Mockito#using-powermock-with-mockito)|

### Testcontainers
¿Testear sin tener que levantar nuestra bd?¿Hacer peticiones a otro servicio sin necesidad de tenerlo accesible?

| <!-- -->    |
|:----:|
|<img src="docs/imgs/logoTestContainers.png" width="150px" alt="Logo de Testcontainers">|
|[Testcontainers](https://www.testcontainers.org/)|
  
### Calidad
Tradicionalmente se ha medido la calidad, teniendo en cuenta los informes de cobertura de código. Tras ver la fragilidad de estas métricas descubrimos el concepto de Mutation Testing Systems.

| <!-- -->    | <!-- -->    |
|:----:|:----:|
| Cobertura | Mutation Testing Systems |
| <img src="docs/imgs/logoJacoco.jpg" width="150px" alt="Logo de Jacoco">|<img src="docs/imgs/logoPitest.png" width="150px" alt="Logo de Pitest">| 
|[JaCoCo](https://www.jacoco.org/jacoco/index.html)|[Pitest](https://pitest.org/)|

### Contract testing
Hemos visto la importancia de tener testeados cada uno de nuestros servicios, pero no debemos olvidarnos de que la comunicación entre ambos también debe ser testeada de alguna forma.

| <!-- -->    | <!-- -->    |
|:----:|:----:|
| <img src="docs/imgs/logoSpringCloud.jpg" width="150px" alt="Logo de Spring Cloud">           | <img src="docs/imgs/logoPact.png" width="150px" alt="Logo de Pact">   |
| [Spring Cloud Contract](https://spring.io/projects/spring-cloud-contract) | [Pact](https://docs.pact.io/)  |



